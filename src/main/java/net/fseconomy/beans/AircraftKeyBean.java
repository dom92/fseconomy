package net.fseconomy.beans;

import java.io.Serializable;
import java.sql.*;
import java.util.Date;

import net.fseconomy.beans.ModelBean.fuelTank;

import net.fseconomy.util.Constants;
import net.fseconomy.util.Converters;
import net.fseconomy.util.Formatters;

public class AircraftKeyBean implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static final int TP_SIMCODER = 1;

    int id;
    Date createdOn;
    int owner;
    int aircraftId;
    String key;
    int thirdParty;

    public AircraftKeyBean(ResultSet rs) throws SQLException
    {
        setId(rs.getInt("id"));
        setCreatedOn(rs.getDate("createdOn"));
        setOwner(rs.getInt("owner"));
        setAircraftId(rs.getInt("owner"));
        setKey(rs.getString("key"));
        setThirdParty(rs.getInt("thirdParty"));
    }

    /**
     * Constructor for AircraftBean.
     */
    public AircraftKeyBean()
    {
        super();
    }

    public void writeBean(ResultSet rs) throws SQLException
    {
        rs.updateString("Key", getKey());
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public void setCreatedOn(Date createdOn)
    {
        this.createdOn = createdOn;
    }

    public Date getCreatedOn()
    {
        return createdOn;
    }

    public void setOwner(int id)
    {
        this.owner = id;
    }

    public int getOwner()
    {
        return owner;
    }

    public void setAircraftId(int aircraftId)
    {
        this.aircraftId = aircraftId;
    }

    public int getAircraftId()
    {
        return aircraftId;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getKey()
    {
        return key;
    }

    public void setThirdParty(int thirdParty)
    {
        this.thirdParty = thirdParty;
    }

    public int getThirdParty()
    {
        return thirdParty;
    }
}
