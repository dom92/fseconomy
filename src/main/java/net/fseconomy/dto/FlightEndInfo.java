package net.fseconomy.dto;

public class FlightEndInfo
{
    public FlightEndInfo(int assignmentsLeft, boolean usedSysCalcFuel, double reportedFuel, double calcFuel)
    {
        this.assignmentsLeft = assignmentsLeft;
        this.usedSysCalcFuel = usedSysCalcFuel;
        this.reportedFuel = reportedFuel;
        this.calcFuel = calcFuel;
    }

    public FlightEndInfo(int assignmentsLeft)
    {
        this.assignmentsLeft = assignmentsLeft;
    }

    public int assignmentsLeft;
    public boolean usedSysCalcFuel = false;
    public double reportedFuel = 0.0;
    public double calcFuel = 0.0;
}
