package net.fseconomy.dto;

public class MapAircraftInfo
{
    public String icao;
    public LatLon latlon;
    public String registration;
    public String makemodel;
    public String equipment;
    public String fuel;
    public String sellprice;
    public String buybackprice;
    public String airframetime;
    public String enginetime;
    public String since100hour;
    public String needsrepair;

    public MapAircraftInfo(String locationIcao, LatLon platlon, String reg, String pMakeModel, String pEquipment,
                           String pTotalFuel, String pSellprice, String pBuybackprice,
                           String pAirframetime, String pEnginetime, String pSince100hour, boolean pNeedsRepair)
    {
        icao = locationIcao;
        latlon = platlon;
        registration = reg;
        makemodel = pMakeModel;
        equipment = pEquipment;
        fuel = pTotalFuel;
        sellprice = pSellprice;
        buybackprice = pBuybackprice;
        airframetime = pAirframetime;
        enginetime = pEnginetime;
        since100hour = pSince100hour;
        needsrepair = pNeedsRepair ? "Yes" : "No";
    }
}
