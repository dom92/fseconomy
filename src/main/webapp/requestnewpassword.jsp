<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import="net.fseconomy.util.Helpers"
%>

<%
    String returnPage = request.getRequestURI();
%>
<jsp:include flush="true" page="/head.jsp" />
</head>
<script>
    function checkSubmit() {
        var btn = document.getElementById("btnSubmit");
        if (checkUser() && checkEmail()) {
            btn.disabled = false;
        }
        else {
            btn.disabled = true;
        }
    }

    function checkEmail() {
        var email = document.getElementById('txtEmail');
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (!filter.test(email.value)) {
            //alert('Please provide a valid email address');
            email.focus;
            return false;
        }
        return true;
    }

    function checkUser() {
        var user = document.getElementById('txtUser');
        var filter = /^[a-zA-Z0-9_.-]{3,45}$/;

        if (!filter.test(user.value)) {
            //alert('Please provide a valid user name');
            user.focus;
            return false;
        }
        return true;
    }
</script>
<body>

<jsp:include flush="true" page="top.jsp" />
<jsp:include flush="true" page="menu.jsp" />

<div id="wrapper">
<div class="content">
<%
    String message = Helpers.getSessionMessage(request);
    if (message != null)
    {
%>
    <p>
        <div class="message"><%= message %></div>
        <br>
        <a href="<%=returnPage%>">Return to Home</a>
    </p>
<%
        return;
    }
    else
    {
%>

    <h2>Login incorrect</h2>
	<p>
	    If you don't remember your password you can request a new password here:
	</p>
	<div class="form" style="width: 400px">
        *all fields required
        <form method="post" action="userctl">
            Username<br>
            <input name="user" id="txtUser" type="text" class="textarea" size="10" onkeyup="checkSubmit()"/><br>
            Email<br>
            <input name="email" id="txtEmail" type="text" class="textarea" size="40"  onkeyup="checkSubmit()"/><br><br>
            <input id="btnSubmit" type="submit" class="btn btn-success" disabled value="Request password" />
            <input type="hidden" name="event" value="password"/>
            <input type="hidden" name="returnpage" value="<%=returnPage%>"/>
        </form>
	</div>
<%
    }
%>
</div>

</div>
</body>
</html>
