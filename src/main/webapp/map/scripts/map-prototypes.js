
function addMapPrototypes()
{
    google.maps.LatLng.prototype.latRadians = function () {
        return (Math.PI * this.lat()) / 180;
    };

    google.maps.LatLng.prototype.lngRadians = function () {
        return (Math.PI * this.lng()) / 180;
    };

    google.maps.LatLng.prototype.distanceFrom = function (newLatLng) {
        // setup our variables
        var radianLat1 = this.latRadians();
        var radianLng1 = this.lngRadians();
        var radianLat2 = newLatLng.latRadians();
        var radianLng2 = newLatLng.lngRadians();

        // sort out the radius, MILES or KM?
        var earth_radius = 3959; // (km = 6378.1) OR (miles = 3959) - radius of the earth

        // sort our the differences
        var diffLat = ( radianLat1 - radianLat2 );
        var diffLng = ( radianLng1 - radianLng2 );

        // put on a wave (hey the earth is round after all)
        var sinLat = Math.sin(diffLat / 2);
        var sinLng = Math.sin(diffLng / 2);

        // maths - borrowed from http://www.opensourceconnections.com/wp-content/uploads/2009/02/clientsidehaversinecalculation.html
        var a = Math.pow(sinLat, 2.0) + Math.cos(radianLat1) * Math.cos(radianLat2) * Math.pow(sinLng, 2.0);

        // work out the distance
        var distance = earth_radius * 2 * Math.asin(Math.min(1, Math.sqrt(a)));

        // return the distance
        return distance;
    };

    // Returns the bearing in degrees between two points.
    // North = 0, East = 90, South = 180, West = 270.
    google.maps.LatLng.prototype.bearing = function bearing(to) {
        const degreesPerRadian = 180.0 / Math.PI;

        // See T. Vincenty, Survey Review, 23, No 176, p 88-93,1975.
        // Convert to radians.
        var lat1 = this.latRadians();
        var lon1 = this.lngRadians();
        var lat2 = to.latRadians();
        var lon2 = to.lngRadians();

        // Compute the angle.
        var angle = -Math.atan2(Math.sin(lon1 - lon2) * Math.cos(lat2), Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));
        if (angle < 0.0)
            angle += Math.PI * 2.0;

        // And convert result to degrees.
        angle = angle * degreesPerRadian;
        angle = angle.toFixed(1);

        return angle;
    };

    /**
     * Returns the zoom level at which the given rectangular region fits in the map view.
     * The zoom level is computed for the currently selected map type.
     * @param {google.maps.Map} map
     * @param {google.maps.LatLngBounds} bounds
     * @return {Number} zoom level
     **/
    google.maps.LatLng.prototype.getZoomByBounds = function getZoomByBounds(map, bounds) {
        var MAX_ZOOM = 15;
        var MIN_ZOOM = 0;

        var ne = map.getProjection().fromLatLngToPoint(bounds.getNorthEast());
        var sw = map.getProjection().fromLatLngToPoint(bounds.getSouthWest());

        var worldCoordWidth = Math.abs(ne.x - sw.x);
        var worldCoordHeight = Math.abs(ne.y - sw.y);

        //Fit padding in pixels
        var FIT_PAD = 40;
        var mapDiv = document.getElementById('map');
        var width = mapDiv.firstElementChild.clientWidth;
        var height = mapDiv.firstElementChild.clientHeight;
        for (var zoom = MAX_ZOOM; zoom >= MIN_ZOOM; --zoom) {
            if (worldCoordWidth * (1 << zoom) + 2 * FIT_PAD < width &&
                worldCoordHeight * (1 << zoom) + 2 * FIT_PAD < height)
                return zoom;
        }
        return 0;
    };
}

