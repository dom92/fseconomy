<%@ page import="net.fseconomy.beans.AssignmentBean" %>
<%@ page import="net.fseconomy.beans.UserBean" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="java.util.*" %>
<%@ page import="net.fseconomy.util.Formatters" %>
<%@ page import="net.fseconomy.beans.AircraftBean" %>
<%@ page import="net.fseconomy.dto.*" %>
<%@ page import="net.fseconomy.data.*" %>
<%@ page import="net.fseconomy.util.GlobalLogger" %>
<%@ page import="net.fseconomy.beans.CachedAirportBean" %>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" %>
<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session"/>
<%
    String type = request.getParameter("type");
    String sId = request.getParameter("id");
    String output;
    int role;

    List<AircraftBean> aircraftList;

    try
    {
        if (type == null)
            throw new Exception("Missing type");

        if (sId == null)
            throw new Exception("Missing Id");

        int id = Integer.parseInt(sId);

        switch (type)
        {
            case "group":

                //check if proper access
                role = Groups.getRole(id, user.getId());
                if (role < UserBean.GROUP_MEMBER)
                {
                    throw new Exception("No permission");
                }
                aircraftList = Aircraft.getAircraftOwnedByUser(id);
                break;
            case "owner":
                if(user.getId() != id)
                {
                    throw new Exception("No permission");
                }
                aircraftList = Aircraft.getAircraftOwnedByUser(id);
                break;
            default:
                throw new Exception("Invalid parameters.");
        }


        Map<String, MapAircraft> acMap = new HashMap<>();
        for (AircraftBean aircraft : aircraftList)
        {
            CachedAirportBean airport = Airports.cachedAirports.get(aircraft.getLocation());

            MapAircraft mapAircraft;
            if(acMap.containsKey(aircraft.getLocation()))
            {
                mapAircraft = acMap.get(aircraft.getLocation());
            }
            else
            {
                mapAircraft = new MapAircraft(new AirportInfo(airport));
            }

            MapAircraftInfo ai = new MapAircraftInfo(
                null,
                null,
                aircraft.getRegistration(),
                aircraft.getMakeModel(),
                aircraft.getSEquipment(),
                (int) (aircraft.getTotalFuel() + .5) + "/" + aircraft.getTotalCapacity() + " Gals",
                "" + aircraft.getSellPrice(),
                "" + aircraft.getSystemSellPrice(),
                Formatters.oneDecimal.format(aircraft.getAirframeHours()),
                Formatters.oneDecimal.format(aircraft.getEngineHours()),
                Formatters.oneDecimal.format(aircraft.getHoursSinceLastCheck()),
                aircraft.isBroken()
            );
            mapAircraft.aircraft.add(ai);
            acMap.put(airport.getIcao(), mapAircraft);
        }

        List<MapAircraft> mapList = new ArrayList<>();
        for( MapAircraft ma : acMap.values())
        {
            mapList.add(ma);
        };
        response.setContentType("application/json");
        Gson gson = new Gson();
        output = gson.toJson(mapList);

    }
    catch (Exception e)
    {
        response.setStatus(400);
        output = "Error: " + e.getMessage();
        GlobalLogger.logJspLog(output);
    }
%><%= output %>