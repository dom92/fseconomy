<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session" />

<%
    if(!user.isLoggedIn())
    {
%>
<script type="text/javascript">document.location.href="/index.jsp"</script>
<%
        return;
    }

    String sType = request.getParameter("type");
    String sId = request.getParameter("id");
    String sTransferId = request.getParameter("transferid");
%>
<jsp:include flush="true" page="/head.jsp" />
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
      integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
      crossorigin=""/>
<!-- Make sure you put this AFTER Leaflet's CSS -->
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
        integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
        crossorigin=""></script>

    <style>
        body {
            padding-top: 50px; /* for nav */
        }

        html, body {
            height: 100%;
            width: 100%;
        }

        #map-canvas {
            height: 100%;
            width: 100%;
        }

        .container-map {
            top: 50px;
            width: 100%;
            height: 100%;
        }
    </style>

    <script>
        var mymap;
        var markers = [];
        function initMap() {
            mymap = L.map('map-canvas', {
                center: [51.505, -0.09],
                zoom: 1
            });

            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1,
                accessToken: 'pk.eyJ1IjoiYWlyYm9zcyIsImEiOiJjazh1d21laHYwZDYyM21xaDd0cnp5NDl6In0.B2XhLGPurGPlKUIIYLC0rA'
            }).addTo(mymap);

            if(type === "group")
                loadAssigments("group", id);
            if(type === "myflight")
                loadAssigments("myflight", 0);
            if(type === "transfer")
                loadAssigments("transfer", id);
            if(type === "aircraftbyowner")
                loadAircraft("owner", id);
            if(type === "aircraftbygroup")
                loadAircraft("group", id);
        }

        function mapAssignments(json) {
            json.mapAssignments.forEach(function(entry){
                createLines(entry);
                createAssignmentMarkers(entry);
            });
            if(type === "myflight" && json.mapAircraftInfo)
                createAircraftMarker(json.mapAircraftInfo);

            setMapBounds();
        }

        function mapAircraft(json) {
            json.forEach(function(entry){
                updateBounds(entry.location.latlon);
                createAircraftMarkers(entry);
            });
            setMapBounds();
        }

        var latMax = 0.0;
        var lonMax = 0.0;
        var latMin = 0.0;
        var lonMin = 0.0;

        function updateBounds(latlon) {
            if(latMax === 0.0){
                latMax = latMin = latlon.lat;
                return;
            }
            if(lonMax === 0.0){
                lonMax = lonMin = latlon.lon;
                return;
            }
            if(latlon.lat > latMax) latMax = latlon.lat;
            if(latlon.lon > lonMax) lonMax = latlon.lon;
            if(latlon.lat < latMin) latMin = latlon.lat;
            if(latlon.lon < lonMin) lonMin = latlon.lon;
        }

        function sortLatLons(){
            var topLeftLat = latMax;
            var botRightLat = latMin;
            var topLeftLon = lonMin;
            var botRightLon = lonMax;

            return [[topLeftLat, topLeftLon],[botRightLat, botRightLon]];
        }
        function setMapBounds(){

            var rect = sortLatLons();

            mymap.fitBounds(rect, {
                padding: [1,1],
                maxZoom: 20
            });
        }

        // Create Markers

        var iconAircraft = L.icon({
            iconUrl: 'img/aircraft.png',

            iconSize:     [32, 37], // size of the icon
            iconAnchor:   [16, 18], // point of the icon which will correspond to marker's location
            popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        });

        var iconLanding = L.icon({
            iconUrl: 'img/airport-landing-up.png',

            iconSize:     [32, 37], // size of the icon
            iconAnchor:   [16, 2], // point of the icon which will correspond to marker's location
        });

        var iconTakeoff = L.icon({
            iconUrl: 'img/airport-takeoff.png',

            iconSize:     [32, 37], // size of the icon
            iconAnchor:   [16, 39], // point of the icon which will correspond to marker's location
        });

        function createAircraftMarker(entry){
            var loc = entry.latlon;

            updateBounds(loc);

            L.marker([loc.lat, loc.lon], {icon: iconAircraft})
                .addTo(mymap)
                .bindPopup(entry.icao + "\n" + entry.registration + "\n" + entry.makemodel + "\n" + entry.equipment + "\n" + entry.fuel);
        }

        function makeAircraftTable(aircraft){
            var aircraftTable = "<p>Aircraft:</p>";
            aircraftTable += "<table border='1' cellpadding='4' style='text-align:center;font-size:10px;'>";
            aircraftTable += "<tr><th>Reg</th><th>MakeModel</th><th>Equip</th><th>Fuel</th><th>Req. Repair</th><th>Sell Price</th><th>Buyback</th></tr>";
            aircraft.forEach(function(item){
                aircraftTable += "<tr><td>"
                    + item.registration + "</td><td>"
                    + item.makemodel + "</td><td>"
                    + item.equipment + "</td><td>"
                    + item.fuel + "</td><td>"
                    + item.needsrepair + "</td><td>"
                    + "$" + item.sellprice + "</td><td>"
                    + "$" + item.buybackprice
                    + "</td></tr>";
            });
            aircraftTable += "</table>";

            return aircraftTable
        }

        function createAircraftMarkers(entry){
            var loc = entry.location.latlon;
            var aircraftTable = makeAircraftTable(entry.aircraft);

            L.marker([loc.lat, loc.lon], {icon: iconTakeoff})
                .addTo(mymap)
                .bindPopup(
                    "<h4>" + entry.location.icao + " - " + entry.location.title + "</h4>" +
                    "<p>" + aircraftTable + "</p>");

        };

        function createLines(entry){
            var departLatLon = entry.departure.latlon;
            var latlngs = [];

            updateBounds(departLatLon);

            entry.destinations.forEach(function(dest){
                var destLatLon = dest.latlon;

                updateBounds(destLatLon);

                latlngs.push([[departLatLon.lat, departLatLon.lon], [destLatLon.lat, destLatLon.lon]])

            });

            var polyline = L.polyline(latlngs, {color: 'blue'}).addTo(mymap);
        }

        function createAssignmentMarkers(entry){
            var loc = entry.departure.latlon;
            var assignmentTable = makeAssignmentTable(entry.departure.icao, entry.assignments);

            updateBounds(loc);

            L.marker([loc.lat, loc.lon], {icon: iconTakeoff})
                .addTo(mymap)
                .bindPopup(
                "<h4>" + entry.departure.icao + " - " + entry.departure.title + "</h4>" +
                "<p>" + assignmentTable + "</p>");

            entry.destinations.forEach(function(dest){
                var loc = dest.latlon;

                updateBounds(loc);

                L.marker([loc.lat, loc.lon], {icon: iconLanding})
                    .addTo(mymap)
                    .bindPopup(dest.icao + " - " + dest.title);
            });
        };

        function makeAssignmentTable(icao, assignments){
            var assignmentTable = "<p>Assignments at <strong>" + icao + "</strong></p>";
            assignmentTable += "<table border='1' cellpadding='4' style='text-align:center;font-size:10px;'>";
            assignmentTable += "<tr><th>ICAO</th><th>Cargo</th><th>Pay</th><th>NM</th></tr>";
            assignments.forEach(function(item){
                assignmentTable += "<tr><td>" + item.icao + "</td><td>" + item.cargo + "</td><td>" + item.pay + "</td><td>" + item.distance + "</td></tr>";
            });
            assignmentTable += "</table>";

            return assignmentTable
        }

        //display selected marker info
        function openInfoWindow(){
            //e.preventDefault();
            //google.maps.event.trigger(vm.selectedItem, 'click');
        }

        //google.maps.event.addDomListener(window, "load", initMap);

    </script>

    <script>
        var type = "<%=sType%>";
        var id = <%=sId == null ? "undefined" : sId %>;

        //load assignments
        function loadAssigments(type, id) {
            $.getJSON("assignmentsjson.jsp", {"type": type, "id": id})
                .success(function(json)
                {
                    console.log("JSON Data Loaded");

                    mapAssignments(json);
                })
                .fail(function(jqxhr, textStatus, error)
                {
                    var err = jqxhr.responseText;
                    alert( "Request Failed: " + err.replace(/^\s+|\s+$/gm, ''));
                });
        }

        //load assignments
        function loadAircraft(type, id) {
            $.getJSON("aircraftjson.jsp", {"type": type, "id": id})
                .success(function(json)
                {
                    console.log("JSON Data Loaded");

                    mapAircraft(json);
                })
                .fail(function(jqxhr, textStatus, error)
                {
                    var err = jqxhr.responseText;
                    alert( "Request Failed: " + err.replace(/^\s+|\s+$/gm, ''));
                });
        }

        $(function () {
            initMap();
        });

    </script>
</head>
<body>
<header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./">FSEconomy</a>
        </div>
        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ul class="nav navbar-nav">

                <%--<li>--%>
                <%--<a href="#">Getting started</a>--%>
                <%--</li>--%>
                <%--<li class="dropdown">--%>
                <%--<a class="dropdown-toggle" href="#" data-toggle="dropdown">Dropdown <b class="caret"></b></a>--%>
                <%--<ul class="dropdown-menu">--%>
                <%--<li><a href="#">Action</a></li>--%>
                <%--<li><a href="#">Another action</a></li>--%>
                <%--<li><a href="#">Something else here</a></li>--%>
                <%--<li><a href="#">Separated link</a></li>--%>
                <%--<li><a href="#">One more separated link</a></li>--%>
                <%--</ul>--%>
                <%--</li>--%>

            </ul>
        </nav>
    </div>
</header>

<div class="container-map">
    <div id="map-canvas"></div>
</div>

</body>
</html>
