<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import="net.fseconomy.beans.*, net.fseconomy.data.*"
%>
<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session" />

<%
    if(!user.isLoggedIn())
    {
%>
<script type="text/javascript">document.location.href="/index.jsp"</script>
<%
        return;
    }

    String content;
    String style = "";
    String sId = request.getParameter("id");
    AircraftMaintenanceBean maintenance = null;
    int id;

    try
    {
        if (sId != null)
        {
            id = Integer.parseInt(sId);
            maintenance = Aircraft.getMaintenance(id);
        }

        if (maintenance == null)
            throw new DataError("No record found.");

        content = "<div class=\"maintenanceLog\">" + maintenance.report() + "</div>";
        style="style=\"background:url(" + maintenance.getFbo().getInvoiceBackground()+ "); background-repeat:no-repeat; height:755px\"";
    }
    catch (DataError e)
    {
        content = "<div class=\"error\">" + e.getMessage() + "</div>";
    }
%>
<jsp:include flush="true" page="/head.jsp" />
</head>
<body <%= style %>>

<div id="wrapper">
<div class="content">
	<%= content %>
</div>
</div>
</body>
</html>
